<?php

/**
* Plugin Name: Hello World plugin
* Plugin URI: http://www.mainwp.com
* Description: This plugin outputs hello world with WordPress
* Version: 1.0.0
* Author: Ryan Guido
* Author URI: http://www.mainwp.com
* License: GPL2
*/

add_action( 'wp_footer', 'my_function' );

function my_function() {
  echo 'Hello world. This is Ryan speaking';
}

?>